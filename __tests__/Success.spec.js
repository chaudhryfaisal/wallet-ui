import React from 'react';
import Success from '../src/client/app/Success';
import renderer from 'react-test-renderer';

describe('Nav', () => {
  it('renders correctly', () => {
    const tree = renderer.create(
      <Success />
    ).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
