'use strict';
require('dotenv').config()
const Hapi = require('hapi');
const Inert = require('inert');
const Path = require('path');
const Hoek = require('hoek');
const Vision = require('vision');
const http = require('http');
const moment = require('moment');
const Good = require('good');

const server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo: Path.join(__dirname, '../client/public')
            }
        }
    }
});

const options = {
    ops: {
        interval: 5000
    },
    reporters: {
        myConsoleReporter: [{
            module: 'good-squeeze',
            name: 'Squeeze',
            args: [{ error: '*'}]
        }, {
            module: 'good-console'
        }, 'stdout'],
        myHTTPReporter: [{
            module: 'good-squeeze',
            name: 'Squeeze',
            args: [{ error: '*' }]
        }]
    }
};

const initJaegerTracer = require('jaeger-client').initTracer;
const opentracing = require('opentracing');
const tracer = new opentracing.Tracer(initTracer('wallet-ui'));
function initTracer(serviceName) {
    var config = {
        'serviceName': serviceName,
        'sampler': {
            'type': 'const',
            'param': 1
        },
        'reporter': {
            'logSpans': true
        }
      };
      var options = {
        'logger': {
            'info': function logInfo(msg) {
                console.log('INFO ', msg);
            },
            'error': function logError(msg) {
                console.log('ERROR', msg)
            }
        }
      };
      return initJaegerTracer(config, options);
}

server.connection({
  host: '0.0.0.0',
  port: process.env.PORT || 3000
});

server.route({
    method: 'GET',
    path: '/',
    handler: function (request, reply) {
        reply.view('index');
    }
});

server.route({
    method: 'GET',
    path: '/health',
    handler: function (request, reply) {
        reply().code(200)
    }
});

server.route({
    method: 'GET',
    path: '/wallets/{email}',
    handler: function (request, reply) {
      const email = request.params.email
      const endpoint = process.env.WALLET_BROKER + email + '/wallets';

      const stringify = JSON.stringify({
        "time": moment().format('YYYY-MM-DDTHH:mm:ssZ'),
        "msg": {
          "subject": "wallet-ui",
          "version": process.env.VERSION,
          "verb": "requests",
          "direct-object": "wallet-broker",
          "indirect-object": "wallets",
          "prep-object": email
        },
        "data": {
          "email": email
        }
      })

      console.log(stringify)

      const span = tracer.startSpan('wallet-broker');
      span.setTag(opentracing.Tags.SAMPLING_PRIORITY, 1);
      span.setTag('wallet-broker', 'wallet-ui');

      http.request(endpoint, res => {
          // output this headers for testings
          console.log(JSON.stringify(res.headers));

          res.setEncoding('utf8');
          res.on('error', err => {
              span.setTag(opentracing.Tags.ERROR, true);
              span.log({ 'event': 'error', 'error.object': err, 'message': err.message, 'stack': err.stack });
              span.finish();
          });
          res.on('data', resp => {
              span.log({ 'event': 'data_received', 'chunk_length': resp.length });
              const chunk = JSON.parse(resp);
              const version = { 'version': process.env.VERSION };
              const results = Object.assign(chunk, version);
              reply(results);
          });
          res.on('end', () => {
              span.log({ 'event': 'request_end' });
              span.finish();
          });
      }).end();
    }
});

server.register(Vision, (err) => {
    Hoek.assert(!err, err);
    server.views({
        engines: {
            html: require('handlebars')
        },
        relativeTo: __dirname,
        path: 'templates',
        layoutPath: './templates/layout',
         layout: 'layout',
    });
});

server.register(Inert, (err) => {
    Hoek.assert(!err, err);
    server.route({
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: '.',
                redirectToSlash: true,
                index: true,
            }
        }
      });
});

server.register({
    register: Good,
    options,
}, (err) => {
    Hoek.assert(!err, err);

    server.start((err) => {
      Hoek.assert(!err, err);
      console.log(`Server running at: ${server.info.uri}`);
    });
});
