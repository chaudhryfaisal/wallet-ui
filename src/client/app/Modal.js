import React from 'react';
import Carousel from './Carousel';
import Shipping from './Shipping';
import Success from './Success';
import Login from './Login';
import axios from 'axios';

class Modal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      success : false,
      auth : false,
      error : false,
      errorMsg: '',
      email : '',
      wallets: []
    };

    this.onClickClose = this.onClickClose.bind(this);
    this.onClickPay = this.onClickPay.bind(this);
    this.onClickLogin = this.onClickLogin.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  onClickPay() {
    this.setState({ success : true });
  }

  onClickLogin() {
    const email = this.state.email
    // Condition to see if email is set
    if( email === '' ) {
      this.setState({ error : true, errorMsg: 'An email address is required.' })
    } else {
      const url = window.location.href
      const endpoint = `${ url }wallets/${email}`

      axios.get(endpoint)
      .then((response) => {
        const { wallets } = response.data

        if(wallets.length > 0) {
          this.setState({ wallets: wallets, auth : true })
        } else {
          this.setState({ error : true, errorMsg: 'No wallets found.' })
        }

      })
      .catch(function (error) {
        this.setState({ auth : false });
        console.error(error)
      });
    }
  }

  handleLogin(e) {
    this.setState({ email: e.target.value})
  }

  onClickClose() {
    this.setState({ success : false, auth : false });
  }

  checkout() {
    return this.state.auth ?
    (
      <div>
        <h4>Pay With</h4>
        <Carousel cards={this.state.wallets} />
        <hr />
        <h4>Ship To</h4>
        <Shipping wallet={this.state.wallets[0]} clickPay={this.onClickPay} />
      </div>
    )
    : <Login
        typeEmail={this.handleLogin}
        clickLogin={this.onClickLogin}
        hasError={this.state.error}
        errorMsg={this.state.errorMsg} />
  }

  render() {
    const successful = this.state.success;
    const checkoutForm = this.checkout();

    return (
      <div>
        <button id="btnOpen" type="button" className="btn btn-secondary" data-toggle="modal" data-target="#monsterModal">
          <span className="skellington-light align-middle mr-2"></span>
          MonsterMesh Pay
        </button>
        <div className="modal fade" id="monsterModal" tabIndex="-1" role="dialog" aria-labelledby="monsterModal" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  <span className="skellington-dark align-middle mr-2"></span>
                  MonsterMesh Pay
                </h5>
                <button onClick={this.onClickClose} type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                { successful
                   ? <Success />
                   : checkoutForm
                }
              </div>
              <div className="modal-footer">
                <button id="btnClose" onClick={this.onClickClose} type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

}

export default Modal;
