import React from 'react';
import {render} from 'react-dom';
import Nav from './Nav';
import Container from './Container';
import Footer from './Footer';

class App extends React.Component {
  render () {
    return (
      <section>
        <Nav />
        <Container />
        <Footer />
      </section>
    )
  }
}

render(<App/>, document.getElementById('app'));
