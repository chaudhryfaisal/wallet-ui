import React from 'react';

const Login = (props) => {
  const errorCondition = props.hasError ?
    (<div className="row">
        <div className="col-md-2"></div>
        <div className="col-md-8">
          <div className="alert alert-danger" role="alert">
            {props.errorMsg}
          </div>
        </div>
    </div>) : '';

  return (
    <div>
        <div className="row">
            <div className="col-md-2"></div>
            <div className="col-md-8">
                <h2>Please Login</h2>
                <hr />
            </div>
        </div>
        { errorCondition }
        <div className="row">
            <div className="col-md-2"></div>
            <div className="col-md-8">
                <div className="form-group has-danger">
                    <label className="sr-only" htmlFor="email">E-Mail Address</label>
                    <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div className="input-group-addon"><i className="fa fa-at"></i></div>
                        <input onChange={props.typeEmail} type="text" name="email" className="form-control" id="email" placeholder="you@example.com" required autoFocus />
                    </div>
                </div>
            </div>
            <div className="col-md-2"></div>
        </div>
        <div className="row">
            <div className="col-md-2"></div>
            <div className="col-md-8">
                <div className="form-group">
                    <label className="sr-only" htmlFor="password">Password</label>
                    <div className="input-group mb-2 mr-sm-2 mb-sm-0">
                        <div className="input-group-addon"><i className="fa fa-key"></i></div>
                        <input type="password" name="password" className="form-control" id="password" placeholder="Password" required />
                    </div>
                </div>
            </div>
            <div className="col-md-2">
                <div className="form-control-feedback">
                    <span className="text-danger align-middle">
                    </span>
                </div>
            </div>
        </div>
        <div className="row">
            <div className="col-md-2"></div>
            <div className="col-md-8">
                <div className="form-check mb-2 mr-sm-2 mb-sm-0">
                    <label className="form-check-label">
                        <input className="form-check-input" name="remember" type="checkbox" />
                        <span>Remember me</span>
                    </label>
                </div>
            </div>
        </div>
        <div className="row mt-3">
            <div className="col-md-2"></div>
            <div className="col-md-8">
                <button id="btnLogin" type="submit" onClick={props.clickLogin} className="btn btn-success"><i className="fa fa-sign-in"></i> Login</button>
            </div>
        </div>
    </div>
  )
};

export default Login;
