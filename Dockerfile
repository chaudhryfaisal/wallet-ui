FROM node:8.9.1-alpine
RUN apk --no-cache add curl
# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# For npm@5 or later, copy package-lock.json as well
COPY package.json package-lock.json ./

RUN npm install --quiet

# Bundle app source
COPY . .

EXPOSE 3000
CMD ["node", "./src/server/index.js"]
